#!/bin/bash
#input.sh - appends lines to an input file

source settings.conf

die () { echo "usage: ./input.sh <servername>"; exit 1; }

[[ ! -z $1 ]] || die

INPUT_FILE="$DATA/$1/$INPUT"

[[ ! -d $INPUT_FILE ]] || die

while echo -n "/admin "; read line; do
    echo $line >> $INPUT_FILE
done