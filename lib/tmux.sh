#!/bin/bash

tmuxify() {
    server=$1

    tmux start-server
    tmux new-session -d -s $server -n window
    tmux split-window -t $server:0
    tmux split-window -h -t $server:0

    tmux send-keys -t $server:0.0 "./run.sh $server" C-m
    tmux send-keys -t $server:0.1 "dialog --inputbox" C-m
    tmux send-keys -t $server:0.2 "sltail -f $DATA/$server/var/ladderlog.txt" C-m


    tmux attach-session -t $server
}
