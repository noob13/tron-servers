# Global Settings

This directory should be symlinked in each `config` folder. To create a symlink, use a command like the one below.

    :::bash
    $ ln -s /path/to/global /path/to/server/config/global

The configs should then be includable in your server configs

    :::bash
    INCLUDE global/config.cfg


