# tron-servers

Scripts for handling multiple [Armagetron Advanced](http://armagetronad.net) servers and configs on a single system.

## Creating a server

    ::bash
    $ ./server add server_1

The `add` command creates the following directory tree:

    :::text    
    servers/server_1
    ├── config
    │   ├── global -> /home/armagetron/servers/global
    │   └── settings_custom.cfg
    ├── data
    │   └── scripts
    ├── input.dat
    ├── log
    └── var

## Running a server

Assuming the game is installed and the paths in `settings.conf` are correctly set, you can run the server like this: 

    :::bash
    $ ./server run -s server_1

This is equivalent to:

    :::bash
    $ ./run -s server_1

Use the `-b` flag to start a server with a different binary:

    :::bash
    $ ./server run -s server_1 -b 1

The number after `-b` corresponds to an index in the `$TRON` array, which can be edited in the `settings.conf` file.

You can automatically start the server in a tmux session by passing it the `-t` option:

    :::bash
    $ ./run -s server_1 -t

## All available commands

    :::text
    usage: ./server <command> [-s] <servername> [-b] 

    command reference:

    run -s <servername> [-b <binary>] [-t].......runs server with name and binary

    (-t starts server in tmux session called <servername>)

    add <servername>.............................creates new server directories
    del <servername>.............................removes server directories

    edit <servername>............................edit settings_custom.cfg
    tree <servername>............................show tree of server files
    list.........................................list available servers

    attach <servername>..........................attach to a tmux session

## Global settings

`add` automatically symlinks a global settings directory to every server. Global configuration files can be included in-game using the command:

    :::text
    /admin include global/killall.cfg  # kill all players on grid 

## Installation

Clone the repository:    

    :::bash
    $ git clone git@bitbucket.org:noob13/tron-servers.git
    
    OR
    
    $ git clone https://bitbucket.org/noob13/tron-servers

Verify binary paths in the `settings.conf` file. It should look something like this:

    :::bash
    TRON[0]='../path/to/armagetronad-dedicated'
    TRON[1]='../path/to/other/binary'